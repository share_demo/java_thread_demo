class Account {

     static synchronized void DorkLuy(String whoDorkLuy) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(whoDorkLuy +" - Dork luy...");
    }
}

class ATMCard extends Thread {
    Account account;
    String whoDorkLuy;
    ATMCard(Account account, String whoDorkLuy) {
        this.account = account;
        this.whoDorkLuy = whoDorkLuy;
    }


    @Override
    public void run() {
        account.DorkLuy(whoDorkLuy);
    }
}

public class Main {

    public static void main(String[] args) {

        Account accountRiel = new Account();
        Account accountDollar = new Account();


        ATMCard atmHusband = new ATMCard(accountRiel, "Husband");
        ATMCard atmWife = new ATMCard(accountDollar, "Wife");

        atmHusband.start();
        atmWife.start();


    }
}
