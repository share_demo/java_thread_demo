
public class Printer {
    String text;

    Printer(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}


class MazerPP extends Thread {
    Printer printer;
    MazerPP(Printer printer) {
        this.printer = printer;
    }

    @Override
    public void run() {
        System.out.println("MzerPP printing...");
        synchronized (printer) {
            System.out.println("Paper is printing...");
            try {
                printer.wait();
                System.out.println(printer.getText());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Finish printing.");
        }
    }
}

class MazerKPS extends Thread {
    Printer printer;
    MazerKPS(Printer printer) {
        this.printer  = printer;
    }

    @Override
    public void run() {
        synchronized (printer) {
            System.out.println("Mazer KPS is put paper");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            printer.notify();
        }
    }
}

class MainClass {
    public static void main(String[] args) {
        Printer printer = new Printer(" MzerPP want to print here");
        MazerPP mazerPP = new MazerPP(printer);
        mazerPP.start();

        MazerKPS mazerKPS = new MazerKPS(printer);
        mazerKPS.start();
    }
}




















